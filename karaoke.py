from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smallsmilhandler import SmallSMILHandler
import sys
import json
import urllib.request


class KaraokeLocal():

    def __init__(self, file_name):

        try:
            parser = make_parser()
            cHandler = SmallSMILHandler()
            parser.setContentHandler(cHandler)
            parser.parse(open(file_name))
            self.tags = cHandler.get_tags()
        except FileNotFoundError:
            sys.exit('No existe el fichero')

    def __str__(self):

        tag_line = ''
        for tag in self.tags:
            tag_line = tag_line + tag[0]
            for elemento in tag[1]:
                if tag[1][elemento] != '':
                    tag_line += '\t' + elemento + '=' + tag[1][elemento]
        # He tenido que usar += ya que me pasaba de numero de caracteres
            tag_line = tag_line + '\n'
        return tag_line

    def to_json(self, filesmil, filejson=''):

        if filejson == '':
            filejson = filesmil.replace('.smil', '.json')

        with open(filejson, 'w') as jsonfile:
            json.dump(self.tags, jsonfile, indent=4)

    def do_local(self):
        for tag in self.tags:
            for elemento in tag[1]:
                if elemento == 'src':
                    if tag[1][elemento].startswith('http'):
                        file_name = tag[1][elemento].split('/')[-1]
                        urllib.request.urlretrieve(tag[1][elemento], file_name)
                        tag[1][elemento] = file_name


if __name__ == '__main__':

    try:
        file_name = sys.argv[1]
    except IndexError:
        sys.exit('Debes usar: python3 karaoke.py file.smil')
    # Modificado el usage: python3 karaoke.py file.smil
    karaoke = KaraokeLocal(file_name)
    print(karaoke)
    karaoke.to_json(file_name)
    karaoke.do_local()
    karaoke.to_json(file_name, 'local.json')
    print(karaoke)
