

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    """Clase con la que podemos usar SMIL"""
    def __init__(self):
        """Constructor para iniciar las variables"""
        self.list = []
        self.att = {'root-layout': ['width', 'height', 'background-color'],
                    'region': ['id', 'top', 'bottom', 'left', 'right'],
                    'img': ['src', 'region', 'begin', 'dur'],
                    'audio': ['src', 'begin', 'dur'],
                    'textstream': ['src', 'region']}

    def startElement(self, name, attrs):
        """Metodo al que se llama cuando se abre una etiqueta"""
        diccionario = {}
        if name in self.att:
            for att in self.att[name]:
                diccionario[att] = attrs.get(att, '')
            self.list.append([name, diccionario])

    def get_tags(self):
        return self.list


if __name__ == '__main__':

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
